import axios from 'axios'
import { Message, Loading } from 'element-ui';
import router from './router'

axios.defaults.baseURL='http://192.168.20.136:7777/';

let loading        //定义loading变量

function startLoading() {    //使用Element loading-start 方法
    loading = Loading.service({
        lock: true,
        text: '加载中...',
        background: 'rgba(0, 0, 0, 0.7)'
    })
}
function endLoading() {    //使用Element loading-close 方法
    loading.close()
}

// 请求拦截  设置统一header
axios.interceptors.request.use(config => {
    // 加载
    startLoading();
    if (localStorage.access_token){//如果有token
    	config.headers.Authorization = localStorage.token_type+" "+localStorage.access_token;
    }else{//没有token，就要请求token
    	config.headers['Authorization'] = "Basic Y2xpZW50OnNlY3JldA==";
    	config.headers['Content-Type'] = "application/x-www-form-urlencoded";//这个设置很重要
    }
    return config;
}, error => {
    return Promise.reject(error)
})

// 响应拦截  401 token过期处理
axios.interceptors.response.use(response => {
    endLoading();//结束加载动画
    return response;
}, error => {
    // 错误提醒
    endLoading();//结束加载动画
    Message.error(error.response.data);

    const { status } = error.response;
    if (status == 401) {
        Message.error('账号密码错误或登录已过期，请重新登录')
        // 清除token
        localStorage.removeItem('access_token');

        // 页面跳转
        router.push('/login');
    }

    return Promise.reject(error)
})

export default axios