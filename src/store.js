import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = { // 需要维护的状态
  user: {}   // 存储用户信息
}

const getters = {
  isAutnenticated: state => state.isAutnenticated,
  user: state => state.user
}

const mutations = {
  setUser(state, user) {
    if (user)
      state.user = user
    else
      state.user = {}
  }
}

const actions = {
  setUserAction: ({ commit }, user) => {
    commit("setUser", user);
  }
}

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})
