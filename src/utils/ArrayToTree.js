export function convert(data, parentId) {
	let vm = this;
	var tree = [];
	var children;
	for(var i = 0; i < data.length; i++) {
		if(data[i].parentId == parentId) {
			var obj = data[i];
			children = convert(data, data[i].id);
			if(children.length > 0) {
				obj.children = children;
				//obj.hasChildren = true;
			}
			tree.push(obj);
		}
	}
	return tree;
}